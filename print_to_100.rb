# A program that prints from 1 to 100

(1..100).each { |num| puts num }

# OR
1.upto(100) { |num| puts num }
