# A program that reads from a CSV file,
# multiply two columns and then writes it back to the CSV file.


require 'csv'

csv_table = CSV.parse(
                  File.read("test.csv"),
                  headers: true,
                  converters: :numeric
                )

csv_table.each do |row|
  row['c'] = row['a'] * row['b']
end
