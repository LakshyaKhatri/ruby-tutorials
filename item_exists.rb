# Checks if an item exists in a given array

puts "Enter array elements seperated by spaces:"
a = gets.chomp.split

puts "Enter element to find:"
x = gets.chomp

exists = false
a.each do |current_num|
  if current_num == x
    exists = true
    break
  end
end

if exists
  puts "Element Exists"
else
  puts "Element Does Not Exists"
end
