# A program to print the frequency of occurrence
# of each character inside an array using Hash.


puts 'Enter elements for array seperated by spaces:'
a = gets.chomp.split

frequency = Hash.new(0)

a.each { |n| frequency[n] = frequency[n] + 1}

puts frequency
