# A program to double given array elements
# with some advanced edge case checking


def double_array_elements(arr)
  arr = arr.clone
  arr.each_with_index do |n, i|
      if n.match?(/^[-+]?[0-9]*$/)
        arr[i] = 2 * n.to_i
  
      elsif n.match?(/^[-+]?[0-9]*\.?[0-9]*$/)
        arr[i] = 2 * n.to_f

      else
        puts "Array element #{n} is not a number."
        puts "Please enter a valid number."
        return
      end
  end

  return arr
end


puts 'Enter elements for array seperated by spaces:'
a = gets.chomp.split
doubled_array = double_array_elements(a)

if doubled_array
  puts doubled_array
end
