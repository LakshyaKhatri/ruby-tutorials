require_relative 'input_handler'
require_relative 'item_factory'
require_relative 'order'


order = Order.new

InputHandler.get_inputs.each do |input|
  item = ItemFactory.generate_item(input)
  order.add_item(item, input['qty'])
end

order.calculate_tax
order.generate_receipt
