require_relative 'order_item'
require_relative 'compute_tax'


class Order

  def initialize
    @order_items = []
    @total = 0
    @total_sales_tax = 0
  end

  def add_item(item, qty)
    @order_items << OrderItem.new(item, qty)
  end

  def calculate_tax
    @order_items.each do |order_item|
      order_item.taxed_price = ComputeTax::get_taxed_price(order_item.item)
    end
  end

  def generate_receipt
    @order_items.each do |order_item|
      puts "#{order_item.qty} #{order_item.item.name}: #{order_item.total}"

      @total += order_item.total
      @total_sales_tax += order_item.sales_tax
    end

    puts "Sales Tax: #{@total_sales_tax.round(2)}"
    puts "Total: #{@total.round(2)}"
  end
end
