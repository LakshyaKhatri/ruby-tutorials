require_relative 'basic_tax'
require_relative 'import_duty'

module ComputeTax
  def self.get_taxed_price(item)
    taxed_price = item.price
    taxes_applied_on(item).each do |tax|
      taxed_price += tax_amount(item.price, tax.rate)
    end

    taxed_price
  end

  def self.taxes_applied_on(item)
    applied_taxes = []
    applied_taxes << BasicTax if item.class == Item
    applied_taxes << ImportDuty if item.is_imported
    applied_taxes
  end

  def self.tax_amount(item_price, tax_rate)
    ((item_price * tax_rate) / 100.0).round(2)
  end

end
