# This should be class or module
# Remember, No object could be created,
# but operations could be added on the class variables
module ItemCategory
  @@BOOKS = 'Books'
  @@FOODS = 'Foods'
  @@MEDS = 'Meds'
  @@OTHERS = 'Others'
end
