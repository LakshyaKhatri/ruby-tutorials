class OrderItem
  attr_reader :item, :qty
  attr_accessor :taxed_price


  def initialize(item, qty)
    @item = item
    @qty = qty
    @taxed_price = @item.price
  end

  def total
    (@taxed_price * qty).round(2)
  end

  def sales_tax
    ((@taxed_price - @item.price) * qty).round(2)
  end
end
