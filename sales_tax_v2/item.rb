class Item
  attr_reader :name, :price, :is_imported

  def initialize(name, price, is_imported)
    @name = name
    @price = price
    @is_imported = is_imported
  end

end
