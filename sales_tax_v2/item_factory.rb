require_relative 'book'
require_relative 'food'
require_relative 'medicine'
require_relative 'item'
require_relative 'item_db'


class ItemFactory
  def self.generate_item(parsed_input)
    item_type = fetch_item_type parsed_input['name']

    item_type.new(
      parsed_input['name'],
      parsed_input['price'],
      parsed_input['is_imported']
    )
  end

  private
  def self.fetch_item_type(name)
    lowercase_name = name.sub(" imported", "").downcase
    return Book if lowercase_name == 'books' || lowercase_name == 'book'
    return Food if ItemDB::FOODS.has_key?(lowercase_name)
    return Medicine if ItemDB::MEDS.has_key?(lowercase_name)
    Item
  end

end
