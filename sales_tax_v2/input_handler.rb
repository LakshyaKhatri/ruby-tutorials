class InputHandler
  @parsed_inputs = []

  def self.get_inputs
    File.foreach("input.txt") { |line| @parsed_inputs << parse_input(line) }
    @parsed_inputs
  end

  private
  def self.parse_input(line)
    item_input = {}
    input_words = line.split
    item_input['qty'] = fetch_qty_from input_words
    item_input['name'] = generate_name_from input_words
    item_input['price'] = fetch_price_from input_words
    item_input['is_imported'] = imported? input_words

    item_input
  end

  private
  def self.fetch_qty_from(input_words)
    input_words[0].to_i
  end

  def self.generate_name_from(input_words)
    # Ignore first word ("<quantity>") and last 2 words ("at <price>")
    input_words[1, input_words.length - 3].join(" ")
  end

  def self.fetch_price_from(input_words)
    input_words[-1].to_f
  end

  def self.imported?(input_words)
    input_words.include? "imported"
  end
end
