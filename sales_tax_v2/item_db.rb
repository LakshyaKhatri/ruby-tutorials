module ItemDB
  FOODS = {
    "chocolates" => true,
    "chocolate bar" => true,
    "box of chocolates" => true
  }

  MEDS = {
    "pain killer" => true,
    "headache pills" => true,
    "antibiotics" => true,
    "packet of headache pills" => true,
  }
end
