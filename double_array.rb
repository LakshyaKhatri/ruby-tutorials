# A program to double given array elements


puts 'Enter elements for array seperated by spaces:'
a = gets.chomp.split

a.each_with_index do |n, i|
  a[i] = 2 * n.to_i
end

puts a
